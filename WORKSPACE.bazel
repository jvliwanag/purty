workspace(
    name = "com_gitlab_joneshf_purty",
)

# Basic loads

load(
    "@bazel_tools//tools/build_defs/repo:http.bzl",
    "http_archive",
)

# Archives and repositories

http_archive(
    name = "bazel_skylib",
    sha256 = "97e70364e9249702246c0e9444bccdc4b847bed1eb03c5a3ece4f83dfe6abc44",
    urls = [
        "https://mirror.bazel.build/github.com/bazelbuild/bazel-skylib/releases/download/1.0.2/bazel-skylib-1.0.2.tar.gz",
        "https://github.com/bazelbuild/bazel-skylib/releases/download/1.0.2/bazel-skylib-1.0.2.tar.gz",
    ],
)

http_archive(
    name = "build_bazel_rules_nodejs",
    sha256 = "d0c4bb8b902c1658f42eb5563809c70a06e46015d64057d25560b0eb4bdc9007",
    urls = [
        "https://github.com/bazelbuild/rules_nodejs/releases/download/1.5.0/rules_nodejs-1.5.0.tar.gz",
    ],
)

http_archive(
    build_file = "//tools/happy:happy.BUILD",
    name = "happy",
    sha256 = "3e81a3e813acca3aae52721c412cde18b7b7c71ecbacfaeaa5c2f4b35abf1d8d",
    strip_prefix = "happy-1.19.9",
    urls = [
        "https://hackage.haskell.org/package/happy-1.19.9/happy-1.19.9.tar.gz",
    ],
)

http_archive(
    name = "rules_haskell",
    patch_args = [
        "-p1",
    ],
    patches = [
        "//:package_root.patch",
    ],
    sha256 = "e1c4e460af15803cd779dd3234140b7d30b0eab669e239383a74c11d23e6be92",
    strip_prefix = "rules_haskell-b9db31a285e697f98a2a9d4afebefb2aacc6b05a",
    urls = [
        "https://github.com/tweag/rules_haskell/archive/b9db31a285e697f98a2a9d4afebefb2aacc6b05a.zip",
    ],
)

# Dependencies

load(
    "@bazel_skylib//:workspace.bzl",
    "bazel_skylib_workspace",
)

bazel_skylib_workspace()

load(
    "@build_bazel_rules_nodejs//:index.bzl",
    "node_repositories",
)

node_repositories()

load(
    "//tools/hlint:deps.bzl",
    "hlint_dependencies",
)

hlint_dependencies()

load(
    "@rules_haskell//haskell:repositories.bzl",
    "rules_haskell_dependencies",
)

rules_haskell_dependencies()

load(
    "//tools/ormolu:deps.bzl",
    "ormolu_dependencies",
)

ormolu_dependencies()

# Toolchains

# Rules

load(
    "@rules_haskell//haskell:cabal.bzl",
    "stack_snapshot",
)

load(
    "@rules_haskell//haskell:toolchain.bzl",
    "rules_haskell_toolchains",
)

rules_haskell_toolchains(
    version = "8.6.5",
)

stack_snapshot(
    local_snapshot = "//:snapshot.yaml",
    name = "stackage",
    packages = [
        "componentm",
        "dlist",
        "exceptions",
        "file-embed",
        "ghc-lib-parser",
        "gitrev",
        "optparse-applicative",
        "ormolu",
        "pathwalk",
        "purescript-cst",
        "rio",
        "syb",
        "tasty",
        "tasty-golden",
        "tasty-hunit",
    ],
    tools = [
        "@alex",
        "@happy",
    ],
)
